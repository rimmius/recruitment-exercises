# Exercise 1
The screenshot of the outcome is located in the root of this repo named `exercise1.png`.

In the `./ex1` folder you will find (hopefully) all the assets you'll need.
Please note that the navbar, the backarrow nor the mapbutton does not need to be included in this exercise.

1.  Create a react native app from the starter kit that you prefer
2.  When the React Native app loads it should do a `GET https://landing.habity.co/recruitment/exercise/1`
3.  The payload and respective field should be used to create the view.
4.  When you feel done with the exercise, zip the code and email it to fredrik[a]habity.co